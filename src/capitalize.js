module.exports = function capitalize(name) {
	if (typeof name !== "string") {
		throw "bad input";
	}
	const nameCapitalized = name.charAt(0).toUpperCase() + name.slice(1);

	return nameCapitalized;
};
